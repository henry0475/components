package log

import "github.com/sirupsen/logrus"

// Register will register a hook to log
func Register(hook logrus.Hook) {
	logger.AddHook(hook)
}

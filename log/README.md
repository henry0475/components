# 日志组件
所有services应该引用此包作为日志输出

## 用法
1. 常规基础用法
```
import (
    // ...
    "gitlab.com/henry0475/components/log"
    // ...
)

// ...

func demo() {
    // ...
    log.System().Error("")
}
```
2. 基础自定义用法
```
import (
    // ...
    "gitlab.com/henry0475/components/log"
    // ...
)

// 全局生效
log.With(log.Option{
    DisableReportCaller: true,  // 禁用调用者的回显
    ENV: Production,            // 设置日志输出时候的环境标识符
})

func demo() {
    // ...
    log.System().Error("")
}
```
3. 含通知的自定义用法
```
import (
    // ...
    "gitlab.com/henry0475/components/log"
    // ...
)

// 全局生效
log.With(log.Option{
    DisableReportCaller: true,  // 禁用调用者的回显
    ENV: Production,            // 设置日志输出时候的环境标识符
})

log.Register(hook)

func demo() {
    // ...
    log.System().Error("")
}
```
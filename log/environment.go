package log

// Environment a new type for distiguishing envs
type Environment string

// String to string
func (e Environment) String() string {
	return string(e)
}

const (
	// Development for developing
	Development Environment = "development"
	// Production for producting
	Production Environment = "production"
)

package log

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	logger = logrus.New()
	option = Option{
		Formatter: &logrus.JSONFormatter{},
		Outer:     os.Stdout,
		ENV:       Development,
	}

	loc   *time.Location
	bjLoc *time.Location // for logger
)

func init() {
	location, err := time.LoadLocation("America/Los_Angeles")
	if err != nil {
		panic(err)
	}
	loc = location
	location, err = time.LoadLocation("Asia/Chongqing")
	if err != nil {
		panic(err)
	}
	bjLoc = location

	setup()
}

func setup() {
	logger.SetReportCaller(!option.DisableReportCaller)
	logger.SetFormatter(option.Formatter)
	for _, hook := range option.Hooks {
		logger.AddHook(hook)
	}
	logger.Out = option.Outer
}

// System for outputting system info
func System(fields ...interface{}) *logrus.Entry {
	_, file, line, _ := runtime.Caller(1)
	params := logrus.Fields{
		"category":    "system",
		"pst_time":    time.Now().In(loc).Format("2006-01-02 15:04:05"),
		"bj_time":     time.Now().In(bjLoc).Format("2006-01-02 15:04:05"),
		"file_name":   fmt.Sprintf("%s:%d", filepath.Base(file), line),
		"package":     filepath.Base(filepath.Dir(file)),
		"environment": option.ENV.String(),
	}
	if len(fields)%2 != 0 {
		// fields should exist in pair
		return logger.WithFields(params)
	}
	for i := 0; i < len(fields); {
		key := "unknown"
		if k, ok := fields[i].(string); ok {
			key = k
		}
		params[key] = fields[i+1]
		i += 2
	}
	return logger.WithFields(params)
}

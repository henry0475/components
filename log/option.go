package log

import (
	"io"

	"github.com/sirupsen/logrus"
)

// Option defines a set of options
type Option struct {
	// DisableReportCaller will remove the report caller info if true
	DisableReportCaller bool
	// Formatter will format the log message as output
	Formatter logrus.Formatter
	// Outer defines where the log info outputs
	Outer io.Writer
	// ENV defines what the current environment is, like development or production
	ENV Environment
	// Hooks 钩子回调
	Hooks []logrus.Hook
}

// With should be used if you want to customize you logger
func With(opts ...Option) {
	for _, opt := range opts {
		if opt.DisableReportCaller {
			option.DisableReportCaller = opt.DisableReportCaller
		}
		if opt.Formatter != nil {
			option.Formatter = opt.Formatter
		}
		if opt.Outer != nil {
			option.Outer = opt.Outer
		}
		if opt.ENV == Production {
			option.ENV = opt.ENV
		}
		if len(opt.Hooks) != 0 {
			option.Hooks = append(option.Hooks, opt.Hooks...)
		}
	}
	// update settings
	setup()
}

package log

import (
	"os"
	"testing"

	"github.com/sirupsen/logrus"
)

func TestWith(t *testing.T) {
	type args struct {
		opts []Option
	}
	tests := []struct {
		name string
		args args
		want Option
	}{
		{
			name: "Test1",
			args: args{
				opts: []Option{
					{DisableReportCaller: true},
				},
			},
			want: Option{
				DisableReportCaller: true,
			},
		},
		{
			name: "Test2",
			args: args{
				opts: []Option{
					{Formatter: &logrus.JSONFormatter{}},
				},
			},
			want: Option{
				Formatter: &logrus.JSONFormatter{},
			},
		},
		{
			name: "Test3",
			args: args{
				opts: []Option{
					{Outer: os.Stdout},
				},
			},
			want: Option{
				Outer: os.Stdout,
			},
		},
	}

	t.Run(tests[0].name, func(t *testing.T) {
		With(tests[0].args.opts...)
		if option.DisableReportCaller != tests[0].want.DisableReportCaller {
			t.Errorf("DisableReportCaller, want: %v, got: %v", tests[0].want.DisableReportCaller, option.DisableReportCaller)
		}
	})
	t.Run(tests[1].name, func(t *testing.T) {
		With(tests[1].args.opts...)
		if option.Formatter == nil {
			t.Errorf("Formatter, want: %v, got: %v", tests[1].want.Formatter, option.Formatter)
		}
	})
	t.Run(tests[2].name, func(t *testing.T) {
		With(tests[2].args.opts...)
		if option.Outer == nil {
			t.Errorf("Outer, want: %v, got: %v", tests[2].want.Outer, option.Outer)
		}
	})
}

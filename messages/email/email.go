package email

import (
	"crypto/tls"
	"fmt"
	"net/smtp"
)

func init() {

}

var option = Option{
	From:        "YumOnTime - Email Center",
	Subject:     "Undefined",
	ContentType: "text/html; charset=UTF-8",
}

type Option struct {
	Host    string
	Port    int
	Account string
	Auth    string

	From, To    string
	Subject     string
	ContentType string
}

// With for options
func With(opts ...Option) {
	for _, opt := range opts {
		if opt.Host != "" {
			option.Host = opt.Host
		}
		if opt.Port != 0 {
			option.Port = opt.Port
		}
		if opt.Account != "" {
			option.Account = opt.Account
		}
		if opt.Auth != "" {
			option.Auth = opt.Auth
		}

		if opt.From != "" {
			option.From = opt.From
		}
		if opt.To != "" {
			option.To = opt.To
		}
		if opt.Subject != "" {
			option.Subject = opt.Subject
		}
		if opt.ContentType != "" {
			option.ContentType = opt.ContentType
		}
	}
}

func Send(msg string, to string) error {
	c, err := client()
	if err != nil {
		return err
	}
	defer c.Close()

	auth := smtp.PlainAuth(
		"", option.Account,
		option.Auth, option.Host,
	)
	if auth != nil {
		if ok, _ := c.Extension("AUTH"); ok {
			if err := c.Auth(auth); err != nil {
				return err
			}
		}
	}
	c.Mail(option.Account)
	if err := c.Rcpt(to); err != nil {
		return err
	}
	w, _ := c.Data()
	w.Write([]byte(createMessage(to, msg)))
	w.Close()
	c.Quit()
	return nil
}

func client() (*smtp.Client, error) {
	conn, err := tls.Dial("tcp", fmt.Sprintf("%s:%d", option.Host, option.Port), nil)
	if err != nil {
		return nil, err
	}
	return smtp.NewClient(conn, option.Host)
}

func createMessage(toEmail string, msg string) string {
	header := make(map[string]string)
	header["From"] = "Developer" + "<" + option.Account + ">"
	header["To"] = toEmail
	header["Subject"] = "Message Service Warning"
	header["Content-Type"] = "text/html; charset=UTF-8"

	var message string
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + "dev.com" + ": " + msg

	return message
}

package certs

import (
	"bytes"
	"crypto/cipher"
	"crypto/des"
	"log"
)

func padding(size int, plainText []byte) []byte {
	nlen := size - len(plainText)%size
	nBytes := bytes.Repeat([]byte{byte(nlen)}, nlen)
	return append(plainText, nBytes...)
}

func unpadding(plainText []byte) []byte {
	nlen := plainText[len(plainText)-1]
	return plainText[:len(plainText)-int(nlen)]
}

func decrypt(key, cipherText []byte) []byte {
	block, _ := des.NewTripleDESCipher(key)
	blockmode := cipher.NewCBCDecrypter(block, iv)
	buf := make([]byte, len(cipherText))
	blockmode.CryptBlocks(buf, cipherText)
	return unpadding(buf)
}

func encrypt(key, plainText []byte) []byte {
	formattedPlainText := padding(8, plainText)
	block, err := des.NewTripleDESCipher(key)
	if err != nil {
		log.Println(err)
	}
	blockmode := cipher.NewCBCEncrypter(block, iv)
	cipherText := make([]byte, len(formattedPlainText))
	blockmode.CryptBlocks(cipherText, formattedPlainText)
	return cipherText
}

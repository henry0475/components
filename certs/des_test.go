package certs

import (
	"encoding/base64"
	"reflect"
	"testing"
)

func Test_encrypt(t *testing.T) {
	type args struct {
		key       []byte
		plainText []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{name: "Test1", args: args{key: []byte(""), plainText: []byte(``)}, want: []byte("aaa")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := encrypt(tt.args.key, tt.args.plainText); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("encrypt() = %v, want %v", base64.StdEncoding.EncodeToString(got), tt.want)
			}
		})
	}
}

func Test_decrypt(t *testing.T) {
	type args struct {
		key        []byte
		cipherText []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{name: "Test1", args: args{key: []byte(""), cipherText: []byte{52, 82, 85, 120, 55, 154, 79, 180}}, want: []byte{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := decrypt(tt.args.key, tt.args.cipherText); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("decrypt() = %s, want %v", got, tt.want)
			}
		})
	}
}

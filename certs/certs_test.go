package certs

import (
	"reflect"
	"testing"
)

func TestRPC_PublicKey(t *testing.T) {
	tests := []struct {
		name    string
		r       RPC
		want    []byte
		wantErr bool
	}{
		{name: "Test1", r: *NewRPC("", Option{ENV: Development}), want: []byte(""), wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.r.PublicKey()
			if (err != nil) != tt.wantErr {
				t.Errorf("RPC.PublicKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RPC.PublicKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

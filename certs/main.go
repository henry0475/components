package certs

import "encoding/base64"

var iv = []byte("tufomn0s")

// URLAddress aliase
type URLAddress string

// String to string
func (u URLAddress) String() string {
	return string(u)
}

const (
	RPCPubKeyAddr URLAddress = "https://www.liwenbin.com/certs/rpc.yumontime.com.crt"
	RPCPriKeyAddr URLAddress = "https://www.liwenbin.com/certs/rpc.yumontime.com.key"

	DevRPCPubKeyAddr URLAddress = "https://www.liwenbin.com/certs/dev.rpc.yumontime.com.crt"
	DevRPCPriKeyAddr URLAddress = "https://www.liwenbin.com/certs/dev.rpc.yumontime.com.key"
)

// RPC for rpc
type RPC struct {
	pkey []byte

	env Environment
}

// PublicKey for public key
func (r RPC) PublicKey() ([]byte, error) {
	res, err := httpGet(r.env.pubKeyURL().String())
	if err != nil {
		return nil, err
	}
	data, err := base64.StdEncoding.DecodeString(string(res))
	if err != nil {
		return nil, err
	}
	return decrypt(r.pkey, data), nil
}

// PrivateKey for private key
func (r RPC) PrivateKey() ([]byte, error) {
	res, err := httpGet(r.env.priKeyURL().String())
	if err != nil {
		return nil, err
	}
	data, err := base64.StdEncoding.DecodeString(string(res))
	if err != nil {
		return nil, err
	}
	return decrypt(r.pkey, data), nil
}

// Endpoint returns a domain where the grpc connections should be connected
func (r RPC) Endpoint() string {
	if r.env == Production {
		return "rpc.yumontime.com"
	}
	return "dev.rpc.yumontime.com"
}

// Environment is a new type of string
type Environment string

func (e Environment) pubKeyURL() URLAddress {
	if e == Production {
		return RPCPubKeyAddr
	}
	return DevRPCPubKeyAddr
}

func (e Environment) priKeyURL() URLAddress {
	if e == Production {
		return RPCPriKeyAddr
	}
	return DevRPCPriKeyAddr
}

const (
	// Development for developing
	Development Environment = "development"
	// Production for producting
	Production Environment = "production"
)

// Option is a set of options
type Option struct {
	ENV Environment
}

// NewRPC will craete a new instance for getting rpc certs
func NewRPC(pkey string, opt Option) *RPC {
	if pkey == "" {
		panic("pkey is empty")
	}
	return &RPC{
		pkey: []byte(pkey),
		env:  opt.ENV,
	}
}

package configs

import (
	"errors"
	"fmt"
	"os"

	"github.com/hashicorp/consul/api"
)

var (
	option = Option{
		DataCenter: "dc1",
		Address:    "configs.yumontime.com:8888",
		Env:        Development,
	}
	c *api.Client
)

// Option an option set
type Option struct {
	Token      string
	DataCenter string
	Address    string
	Env        Environment
}

// Ready determines whether the client can be made or not
func (o Option) Ready() bool {
	if o.Token == "" || o.Address == "" {
		return false
	}
	if o.DataCenter == "" {
		return false
	}
	return true
}

var (
	// ErrKeyNotExist for invalid key provided
	ErrKeyNotExist = errors.New("the key does not exist")
	// ErrNotInitialized for uninitialized
	ErrNotInitialized = errors.New("the client does not be initialized, call With() first")
	// ErrEmptyToken missing token
	ErrEmptyToken = errors.New("missing a valid token for accessing")
	// ErrNotReady missing some params
	ErrNotReady = errors.New("missing some params")
)

func init() {
	if token := os.Getenv("YOT_CONFIGS_TOKEN"); token != "" {
		option.Token = token
	}
	// try to make a client
	// error is ignored since the With() can reconnect later
	_ = client()
}

// Environment defines different envs
type Environment string

// String to string
func (e Environment) String() string {
	return string(e)
}

const (
	// Development for developing
	Development Environment = "development"
	// Production for producting
	Production Environment = "production"
)

// With will customize your client
func With(opts ...Option) error {
	for _, opt := range opts {
		if opt.Token != "" {
			option.Token = opt.Token
		}
		if opt.DataCenter != "" {
			option.DataCenter = opt.DataCenter
		}
		if opt.Address != "" {
			option.Address = opt.Address
		}
		if opt.Env == Production {
			option.Env = Production
		}
	}
	// recreate client
	return client()
}

// Set will push a config pair to consul
func Set(key, val string) error {
	if c == nil {
		return ErrNotInitialized
	}
	_, err := c.KV().Put(
		&api.KVPair{
			Key:   fmt.Sprintf("%s/%s", option.Env.String(), key),
			Value: []byte(val),
		}, nil,
	)
	if err != nil {
		return err
	}
	return nil
}

// Get returns the value of the key from consul
func Get(key string) (string, error) {
	if c == nil {
		return "", ErrNotInitialized
	}
	data, _, err := c.KV().Get(
		fmt.Sprintf("%s/%s", option.Env.String(), key),
		nil,
	)
	if err != nil {
		return "", err
	}
	if data == nil {
		return "", ErrKeyNotExist
	}
	return string(data.Value), nil
}

func client() error {
	if !option.Ready() {
		return ErrNotReady
	}
	cfg := api.DefaultConfig()
	cfg.Datacenter = option.DataCenter
	cfg.Token = option.Token
	cfg.Address = option.Address
	client, err := api.NewClient(cfg)
	if err != nil {
		return err
	}
	c = client
	return nil
}

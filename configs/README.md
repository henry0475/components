# 配置管理
配置分为两套，分别为Development和Production，由配置项的Env参数指定。
如：
```
// 开发环境的配置信息
configs.With(configs.Option{
    Env: configs.Development,
})
// 生产环境的配置信息
configs.With(configs.Option{
    Env: configs.Production,
})
```

## 基础使用
1. 导入包
```
go get "gitlab.com/henry0475/components/configs"
```
2. 通过环境变量`YOT_CONFIGS_TOKEN`设置访问Token
3. 开始使用
```
// testSet 测试set
func testSet() {
    if err := configs.Set("key", "val"); err != nil {
        // handle error
    }
}
// testGet 测试get
func testGet() {
    val, err := configs.Get("key")
    if err != nil {
        // handle error
    }
    log.Printf(val)
}
```

## 配置项
配置项使用With()函数进行设置，但需要注意
- 具有最高优先级，会覆盖环境变量的设置
- 调用完With()后，组件会重新创建client()；如果设置的值不合法，会导致client创建失败
```
func main() {
    // 可以判定With()函数的返回值，如果为error则代表client创建失败，可能因为参数问题
    configs.With(configs.Option{
        Token: "",
        DataCenter: "",
        Address: "",
        Env: configs.Development,
    })
    val, err := configs.Get("key")
    // ....
}
```

## 规范
1. 设置配置项key的时候，需要基于目录配置，均小写，并且使用下划线区分多个单词，如coupon模块的配置信息设置：
```
if err := configs.Set("services/coupon/expire_times", "val"); err != nil {
    // handle error
}
```
2. 设置配置信息时候，通常我们是在Development设置的，此时需要同步一份至Production。可以去线上配置or联系Henry.
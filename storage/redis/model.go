package redis

import (

	"github.com/go-redis/redis/v8"
)

type Z redis.Z

// type ZRangeBy redis.ZRangeBy
var ZRangeBy = redis.ZRangeBy{}

type Sort redis.Sort

type ZWithKey redis.ZWithKey
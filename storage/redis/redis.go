package redis

import (
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis/v8"
)

type Option struct {
	Address  string `json:"address"`
	DB       int    `json:"db"`
	Username string `json:"username"`
	Password string `json:"password"`
	Port     int    `json:"port"`
	Network  string `json:"network"`

	// Maximum number of socket connections.
	// Default is 10 connections per every CPU as reported by runtime.NumCPU.
	// PoolSize int `json:"pool_size"`

	// Minimum number of idle connections which is useful when establishing. new connection is slow.
	// MinIdleConns int `json:"min_idle_conns"`

	// Maximum number of retries before giving up.
	// Default is 3 retries; -1 (not 0) disables retries.
	MaxRetries int `json:"max_retries"`
}

var (
	conn   *redis.Client
	option = Option{
		MaxRetries: 3,
	}
)
const Nil = redis.Nil

// With makes customized settings
func With(opts ...Option) error {

	if conn != nil {
		conn.Close()
	}
	for _, opt := range opts {
		if opt.Address != "" {
			option.Address = opt.Address
		}
		if opt.DB != 0 {
			option.DB = opt.DB
		}
		if opt.Username != "" {
			option.Username = opt.Username
		}
		if opt.Password != "" {
			option.Password = opt.Password
		}
		if opt.Port != 0 {
			option.Port = opt.Port
		}
		if opt.MaxRetries != 0 {
			option.MaxRetries = opt.MaxRetries
		}
	}
	return establish()
}

func dataSource() *redis.Options {
	cfg := &redis.Options{
		Network:    "tcp",
		Addr:       fmt.Sprintf("%s:%d", option.Address, option.Port),
		Username:   option.Username,
		Password:   option.Password,
		DB:         option.DB,
		MaxRetries: option.MaxRetries,
	}

	return cfg
}

func establish() error {
	conn = redis.NewClient(dataSource())

	pong, err := conn.Ping(conn.Context()).Result()
	if err != nil || pong != "PONG" {
		return err
	}

	return nil
}

// Load will load json string to unmarshal options
func Load(src []byte) error {
	if err := json.Unmarshal(src, &option); err != nil {
		return err
	}
	return establish()
}

// Redis can return nil if Load() or With() has errors
func Redis() *redis.Client {
	return conn
}

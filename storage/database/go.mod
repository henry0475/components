module gitlab.com/henry0475/components/storage/database

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.3
	github.com/go-sql-driver/mysql v1.6.0
)

// Package database contains a set of entrences for operating databses like MySQL, Redis, and so on
package database

import (
	"database/sql"

	"github.com/go-redis/redis/v8"
)

var (
	mRedis = newRedisManager()
	mMySQL = newMySQLManager()
)

// Load will load json string to unmarshal options
func Load(src []byte) error {
	if err := mMySQL.load(src); err != nil {
		return err
	}
	return mMySQL.establish()
}

// LoadMySQL will load json string to unmarshal options for MySQL
func LoadMySQL(src []byte) error {
	if err := mMySQL.load(src); err != nil {
		return err
	}
	return mMySQL.establish()
}

// MySQL can return nil if LoadMySQL() has errors
func MySQL() *sql.DB {
	return mMySQL.conn
}

// ValidMySQL returns a bool value showed whether the mysql is useable
func ValidMySQL() bool {
	if mMySQL == nil {
		return false
	}
	if mMySQL.conn == nil {
		return false
	}
	if err := mMySQL.conn.Ping(); err != nil {
		return false
	}
	return true
}

// LoadRedis will load json string to unmarshal options for redis
func LoadRedis(src []byte) error {
	if err := mRedis.load(src); err != nil {
		return err
	}
	return mRedis.establish()
}

// Redis can return nil if LoadRedis() has errors
func Redis() *redis.Client {
	return mRedis.conn
}

// ValidRedis returns a bool value showed whether the redis is useable
func ValidRedis() bool {
	if mRedis == nil {
		return false
	}
	if mRedis.conn == nil {
		return false
	}
	pong, err := mRedis.conn.Ping(mRedis.conn.Context()).Result()
	if err != nil || pong != "PONG" {
		return false
	}
	return true
}

// Destory can be used for closing connections
func Destory() {
	mMySQL.destory()
	mRedis.destory()
}

package database

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

type MySQLOption struct {
	Address  string `json:"address"`
	DBName   string `json:"dbname"`
	Username string `json:"username"`
	Password string `json:"password"`
	Port     int    `json:"port"`

	// MaxOpenConns sets the maximum number of open connections to the database
	MaxOpenConns int `json:"max_open_conns"`
	// MaxIdleConns sets the maximum number of connections in the idle connection pool.
	MaxIdleConns int `json:"max_idle_conns"`
}

type mysqlManager struct {
	conn   *sql.DB
	option MySQLOption
}

func (m *mysqlManager) load(src []byte) error {
	if err := json.Unmarshal(src, &m.option); err != nil {
		return err
	}
	return nil
}

func (m mysqlManager) dataSource() string {
	cfg := mysql.Config{
		Addr:                    fmt.Sprintf("%s:%d", m.option.Address, m.option.Port),
		User:                    m.option.Username,
		Passwd:                  m.option.Password,
		Net:                     "tcp",
		DBName:                  m.option.DBName,
		AllowNativePasswords:    true,
		AllowCleartextPasswords: true,
	}
	return cfg.FormatDSN()
}

func (m *mysqlManager) establish() error {
	m.destory()

	db, err := sql.Open("mysql", m.dataSource())
	if err != nil {
		return err
	}
	if err := db.Ping(); err != nil {
		return err
	}
	db.SetMaxOpenConns(m.option.MaxOpenConns)
	db.SetMaxIdleConns(m.option.MaxIdleConns)
	m.conn = db
	return nil
}

func (m *mysqlManager) destory() {
	if m.conn != nil {
		m.conn.Close()
	}
}

func newMySQLManager() *mysqlManager {
	return &mysqlManager{
		option: MySQLOption{
			MaxOpenConns: 20,
			MaxIdleConns: 3,
		},
	}
}

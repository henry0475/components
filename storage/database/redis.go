package database

import (
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis/v8"
)

type RedisOption struct {
	Address  string `json:"address"`
	DB       int    `json:"db"`
	Username string `json:"username"`
	Password string `json:"password"`
	Port     int    `json:"port"`
	Network  string `json:"network"`

	// Maximum number of socket connections.
	// Default is 10 connections per every CPU as reported by runtime.NumCPU.
	// PoolSize int `json:"pool_size"`

	// Minimum number of idle connections which is useful when establishing. new connection is slow.
	// MinIdleConns int `json:"min_idle_conns"`

	// Maximum number of retries before giving up.
	// Default is 3 retries; -1 (not 0) disables retries.
	MaxRetries int `json:"max_retries"`
}

type redisManager struct {
	conn   *redis.Client
	option RedisOption
}

func (r *redisManager) load(src []byte) error {
	if err := json.Unmarshal(src, &r.option); err != nil {
		return err
	}
	return nil
}

func (r redisManager) dataSource() *redis.Options {
	cfg := &redis.Options{
		Network:    "tcp",
		Addr:       fmt.Sprintf("%s:%d", r.option.Address, r.option.Port),
		Username:   r.option.Username,
		Password:   r.option.Password,
		DB:         r.option.DB,
		MaxRetries: r.option.MaxRetries,
	}
	return cfg
}

func (r *redisManager) establish() error {
	r.destory()

	r.conn = redis.NewClient(r.dataSource())
	pong, err := r.conn.Ping(r.conn.Context()).Result()
	if err != nil || pong != "PONG" {
		return err
	}
	return nil
}

func (r *redisManager) destory() {
	if r.conn != nil {
		r.conn.Close()
	}
}

func newRedisManager() *redisManager {
	return &redisManager{
		option: RedisOption{MaxRetries: 3},
	}
}

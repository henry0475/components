package endpoint

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
)

// Endpoint 定义的接入点
type Endpoint struct {
	Name  string
	Point string
	Port  int
}

// String 返回其接入点
func (e Endpoint) String() string {
	return fmt.Sprintf("%s:%d", e.Point, e.Port)
}

var (
	endpoints = make(map[string]Endpoint)
	mu        sync.RWMutex
)

var (
	// ErrInvalidServiceName 配置文件未同步
	ErrInvalidServiceName = errors.New("missing service defines")
)

// Load 加载配置文件
func Load(src []byte) error {
	mu.Lock()
	defer mu.Unlock()

	var cfg struct {
		Endpoints []struct {
			Name     string `json:"name"`
			Endpoint string `json:"endpoint"`
			Port     int    `json:"port"`
		} `json:"endpoints"`
	}
	if err := json.Unmarshal(src, &cfg); err != nil {
		return err
	}
	for _, s := range cfg.Endpoints {
		endpoints[s.Name] = Endpoint{
			Name:  s.Name,
			Point: s.Endpoint,
			Port:  s.Port,
		}
	}
	return nil
}

// Get 基于服务名称，返回相对应的服务接入点
func Get(name string) (Endpoint, error) {
	mu.RLock()
	defer mu.RUnlock()

	if point, ok := endpoints[name]; ok {
		return point, nil
	}
	return Endpoint{}, ErrInvalidServiceName
}

package uniqueid

import (
	"testing"
)

func TestGetUniqueID(t *testing.T) {
	err := Register()
	if err != nil {
		t.Error(err)
	}

	nodeMap := make(map[int64]int)
	for i := 1; i < 10000; i++ {
		uniqueID := Number()
		if id, ok := nodeMap[uniqueID]; ok {
			t.Error(id)
		} else {
			nodeMap[uniqueID] = i
		}
	}
	t.Log(nodeMap)
	t.Log(String())
}

package loghook

import (
	"encoding/json"

	"github.com/sirupsen/logrus"
	"gitlab.com/henry0475/components/loghook/slack"
)

// Platform defines different hooks
type Platform string

// String to string
func (p Platform) String() string {
	return string(p)
}

const (
	//Slack for slack uses
	Slack Platform = "slack"
)

var (
	hooks = make(map[Platform]Hook)
)

// Load for parsing and load cfg
func Load(src []byte) error {
	var cfg struct {
		Platforms []struct {
			Name     string `json:"name"`
			Endpoint string `json:"endpoint"`
			Secret   string `json:"secret"`
		} `json:"platforms"`
	}
	if err := json.Unmarshal(src, &cfg); err != nil {
		return err
	}

	for _, pt := range cfg.Platforms {
		if Platform(pt.Name) == Slack {
			hooks[Platform(pt.Name)] = slack.New(pt.Endpoint)
		}
	}
	return nil
}

type Hook interface {
	Logrus() logrus.Hook
}

func Get(platform Platform) Hook {
	return hooks[platform]
}

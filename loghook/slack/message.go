package slack

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
)

type block struct {
	Type      string `json:"type"`
	Text      field  `json:"text,omitempty"`
	BlockID   string `json:"block_id,omitempty"`
	Accessory struct {
		Type     string `json:"type"`
		ImageURL string `json:"image_url"`
		AltText  string `json:"alt_text"`
	} `json:"accessory,omitempty"`
	Fields []field `json:"fields,omitempty"`
}

type field struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type Message struct {
	Blocks []block `json:"blocks"`
}

func NewMessage(entry *logrus.Entry) *Message {
	title := block{
		Type: "section",
		Text: field{
			Type: "mrkdwn",
			Text: fmt.Sprintf("System Monitor - *%s*", entry.Level.String()),
		},
	}
	var s strings.Builder
	for k, val := range entry.Data {
		if v, ok := val.(string); ok {
			s.WriteString(k + ": " + v + "\n")
		}
	}
	body := block{
		Type: "section",
		Text: field{
			Type: "mrkdwn",
			Text: s.String(),
		},
	}
	extra := block{
		Type: "section",
		Text: field{
			Type: "mrkdwn",
			Text: entry.Message,
		},
	}

	msg := &Message{}
	msg.Blocks = append(msg.Blocks, title, body, extra)
	return msg
}

package slack

import "github.com/sirupsen/logrus"

type Slack struct {
	endpoint string
}

func (s Slack) Logrus() logrus.Hook {
	return s
}

// Levels ...
func (s Slack) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
	}
}

// Fire sends message out
func (s Slack) Fire(entry *logrus.Entry) error {
	if _, err := postRequestJSON(s.endpoint, NewMessage(entry)); err != nil {
		return err
	}
	return nil
}

func New(url string) *Slack {
	return &Slack{
		endpoint: url,
	}
}

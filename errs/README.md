# 

## 可选基础用法
```
import (
    // ...
    "gitlab.com/henry0475/components/errs"
    // ...
)

func main() {
    // ...
    errs.Register(errs.Option{ModName: "TestMod"})
    // ...
}

func name() error {
    // ...
    return errs.New(1001, "test msg")
}
```

## 推荐用法
```
import (
    // ...
    "gitlab.com/henry0475/components/errs"
    // ...
)

var (
    ErrNameA = errs.New(1001, "test msg")
    ErrNameB = errs.New(1002, "test msg")
    ErrNameC = errs.New(1003, "test msg")
)

func main() {
    // ...
    errs.Register(errs.Option{ModName: "TestMod"})
    // ...
}

func name() error {
    // ...
    return ErrNameA
}
```
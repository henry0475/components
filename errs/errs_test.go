package errs

import (
	"errors"
	"reflect"
	"testing"
)

func TestParse(t *testing.T) {
	type args struct {
		err error
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{name: "Test1", args: args{errors.New(`{"code":111, "mod": "test", "msg": "abc}`)}, want: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Parse(tt.args.err); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() = %v, want %v", got, tt.want)
			}
		})
	}
}

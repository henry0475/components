package errs

import (
	"encoding/json"
)

var option = Option{
	ModName: "undefined",
}

type Error interface {
	Error() string
	Code() int
}

// Err new type of error
type Err struct {
	Mod string `json:"mod"`
	C   int    `json:"code"`
	Msg string `json:"msg"`
}

// Option customized
type Option struct {
	ModName string
}

// Error will format the output
func (e Err) Error() string {
	e.Mod = option.ModName
	out, _ := json.Marshal(e)
	return string(out)
}

// Code returns the code of error
func (e Err) Code() int {
	return e.C
}

// Register should be called before use in main()
func Register(opts ...Option) {
	for _, opt := range opts {
		if opt.ModName != "" {
			option.ModName = opt.ModName
		}
	}
}

// New returns a new type of error
func New(code int, msg string) Err {
	return Err{
		C:   code,
		Msg: msg,
	}
}

// Turn should be used if a normal error obtained, you want to turn to Err type
func Turn(code int, err error) Err {
	return Err{
		C:   code,
		Msg: err.Error(),
	}
}

// Parse ...
func Parse(err error) Error {
	var e = Err{}
	if err := json.Unmarshal([]byte(err.Error()), &e); err != nil {
		return nil
	}
	return e
}

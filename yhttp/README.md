# HTTP client组件

在原生的http client上做一些简单的封装，使得在调用http请求时变得格外方便。

可以使用链式操作，追加header像方法调用一样简单

## 用法

1. Get请求，带Header

```plaintext
import (
    // ...
    "gitlab.com/henry0475/components/yhttp"
    // ...
)

// ...

func TestGet(t *testing.T) {

	ctx := context.Background()
	req, err := NewClient(ctx).Header("Authorization","ey").Get("http://dev.yumontime.com/api/v1/xxx")

	if err != nil {
		t.Error(err)
	}

	t.Log(string(req))
}
```

2. Post请求，带Header

```plaintext
import (
    // ...
    "gitlab.com/henry0475/components/yhttp"
    // ...
)


func TestPost(t *testing.T) {

	ctx := context.Background()
	// 只是做demo测试，接口参数可能不对
	req, err := NewClient(ctx).Header("Authorization","ey").Post("http://127.0.0.1:5000/api/v1/xxxx", map[string]interface{}{
		"orderID": 308645277755271,
		"score":   1,
	})

	if err != nil {
		t.Error(err)
	}

	t.Log(string(req))

}
```

3. PUT请求，带Header

   todo

4. DELETE请求，带header

   todo



## 未来计划

- [ ] 实现基础的POST,GET,DELETE,PUT请求；
- [x] 自定义header；
- [ ] 参数格式**Content-Type**实现自定义；
- [ ] 超时控制；
- [ ] 重连机制；
- [ ] ...


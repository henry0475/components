package yhttp

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strings"
)

type yclient struct {
	client  *http.Client
	header  *http.Header
	cookies *http.Cookie
	prefix  string // Prefix for request.
}

var client = &yclient{
	client: &http.Client{},
	header: &http.Header{},
}

var defaultTrimChars = string([]byte{
	'\t', // Tab.
	'\v', // Vertical tab.
	'\n', // New line (line feed).
	'\r', // Carriage return.
	'\f', // New page.
	' ',  // Ordinary space.
	0x00, // NUL-byte.
	0x85, // Delete.
	0xA0, // Non-breaking space.
})

func Client() *yclient {
	return client
}

// send get request
func (c yclient) Get(ctx context.Context, urlStr string) ([]byte, error) {
	if len(c.prefix) > 0 {
		urlStr = c.prefix + strings.Trim(urlStr, defaultTrimChars)
	}
	req, err := http.NewRequestWithContext(ctx, "GET", urlStr, nil)
	if err != nil {
		return nil, err
	}
	if c.header != nil {
		req.Header = *c.header
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

// send post request
func (c yclient) Post(ctx context.Context, urlStr string, data ...interface{}) ([]byte, error) {
	if len(c.prefix) > 0 {
		urlStr = c.prefix + strings.Trim(urlStr, defaultTrimChars)
	}

	var payload io.Reader
	if len(data) > 0 {
		switch c.header.Get("Content-Type") {
		case "application/json":
			var params string
			switch data[0].(type) {
			case string, []byte:
				params = fmt.Sprint(data[0])
			default:
				if b, err := json.Marshal(data[0]); err != nil {
					return nil, err
				} else {
					params = string(b)
				}
			}
			payload = strings.NewReader(params)
		default:
			payloadBuf := &bytes.Buffer{}
			writer := multipart.NewWriter(payloadBuf)
			// Set the default 'Content-Type'
			if c.header.Get("Content-Type") == "" {
				c.header.Set("Content-Type", writer.FormDataContentType())
			}

			if dataMap, ok := data[0].(map[string]interface{}); ok {
				for k, v := range dataMap {
					_ = writer.WriteField(k, fmt.Sprint(v))
				}
				if err := writer.Close(); err != nil {
					return nil, err
				}
			}
			payload = payloadBuf
		}
	}

	req, err := http.NewRequestWithContext(ctx, "POST", urlStr, payload)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", c.header.Get("Content-Type"))
	req.AddCookie(c.cookies)
	if c.header != nil {
		req.Header = *c.header
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

// Setting the header parameter should be 'map[string]string{}' or '[]string{}'
// Usually you need to set up 'Content-Type'
// Example:
// c.Headers([]string{"key","value"})
// c.Headers(map[string]string{"key":"value"})
func (c yclient) SetHeader(args interface{}) yclient {

	if argsMap, ok := args.(map[string]string); ok {
		for k, v := range argsMap {
			c.header.Add(k, v)
		}
		return c
	}

	if argsSlice, ok := args.([]string); ok {
		if len(argsSlice)%2 != 0 {
			return c
		}
		for i := 1; i < len(argsSlice); i += 2 {
			c.header.Add(argsSlice[i-1], argsSlice[i])
		}
	}

	return c
}

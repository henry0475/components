package yhttp

import (
	"context"
	"encoding/json"
	"testing"
)

func TestGet(t *testing.T) {
	Client().prefix = "https://v2.jinrishici.com/"
	// Client().SetHeader(map[string]string{"key": "value"})
	ctx := context.Background()
	req, err := Client().Get(ctx, "token")

	if err != nil {
		t.Error(err)
	}

	t.Log(string(req))
	token := map[string]string{}

	json.Unmarshal(req, &token)

	req, err = Client().SetHeader([]string{"X-User-Token", token["data"]}).Get(ctx, "sentence")
	t.Log(Client().header.Get("X-User-Token"))
	t.Log(string(req), err)
}

func TestPost(t *testing.T) {

	Client().prefix = "https://v2.jinrishici.com"
	body, err := Client().Get(context.TODO(), "/token")

	t.Error(string(body), err)

}
